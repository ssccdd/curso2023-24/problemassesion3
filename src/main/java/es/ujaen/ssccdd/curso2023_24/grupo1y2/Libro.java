package es.ujaen.ssccdd.curso2023_24.grupo1y2;

import java.util.Date;

public class Libro implements Comparable<Libro> {
    private final String iD;
    private final String titulo;
    private Date fechaPrestamo;

    public Libro(String iD, String titulo) {
        this.iD = iD;
        this.titulo = titulo;
        this.fechaPrestamo = new Date();
    }

    public String getiD() {
        return iD;
    }

    public String getTitulo() {
        return titulo;
    }

    public Date getFechaPrestamo() {
        return fechaPrestamo;
    }

    public void setFechaPrestamo(Date fechaPrestamo) {
        this.fechaPrestamo = fechaPrestamo;
    }

    @Override
    public String toString() {
        return "Libro{" +
                "iD='" + iD + '\'' +
                ", titulo='" + titulo + '\'' +
                ", fechaPrestamo=" + fechaPrestamo +
                '}';
    }

    @Override
    public int compareTo(Libro libro) {
        return this.iD.compareTo(libro.getiD());
    }
}
