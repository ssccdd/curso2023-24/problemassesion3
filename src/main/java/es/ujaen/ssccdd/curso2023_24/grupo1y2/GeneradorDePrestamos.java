package es.ujaen.ssccdd.curso2023_24.grupo1y2;

import java.util.Queue;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

import static es.ujaen.ssccdd.curso2023_24.grupo1y2.Constantes.aleatorio;

class GeneradorDePrestamos implements Runnable {
    private static final int NUM_PRESTAMOS = 3;
    private static final int TIEMPO_GENERACION = 5;
    private final String iD;
   private final Queue<Libro> listaPrestamos;
   private final CyclicBarrier sincGeneracion;
   private int secuencia;

    public GeneradorDePrestamos(String iD, Queue<Libro> listaPrestamos, CyclicBarrier sincGeneracion) {
        this.iD = iD;
        this.listaPrestamos = listaPrestamos;
        this.sincGeneracion = sincGeneracion;
        this.secuencia = 0;
    }

    public String getiD() {
        return iD;
    }

    @Override
    public void run() {
        System.out.println("Hilo("+iD+") comienza su ejecución");
        try {
            while (true)
                prepararPrestamos();
        } catch (InterruptedException | BrokenBarrierException e) {
            System.out.println("Hilo("+iD+") ha FINALIZADO");
        }
    }

    /**
     * Esperará a preparar un número de préstamos hasta que su gestor se lo indique.
     * A la finalización se sincronizará con su gestor para indicar que ha terminado.
     * @throws BrokenBarrierException
     * @throws InterruptedException
     */
    private void prepararPrestamos() throws BrokenBarrierException, InterruptedException {
        System.out.println("Hilo("+iD+") espera para crear nuevos préstamos");
        sincGeneracion.await(); // Espera a que el gestor le pida más préstamos
        int numPrestamos = aleatorio.nextInt(NUM_PRESTAMOS) + 1;
        int tiempo = aleatorio.nextInt(TIEMPO_GENERACION);
        for (int i = 0; i < numPrestamos; i++) {
            Libro nuevoPrestamo = new Libro(iD+"-Libro-"+secuencia, "Libro-" + secuencia);
            secuencia++;
            listaPrestamos.add(nuevoPrestamo);
        }
        TimeUnit.SECONDS.sleep(tiempo);
        sincGeneracion.await();
        System.out.println("Hilo("+iD+") ha preparado " + numPrestamos + " nuevos préstamos");
    }
}
