package es.ujaen.ssccdd.curso2023_24.grupo1y2;

import java.util.*;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

import static es.ujaen.ssccdd.curso2023_24.grupo1y2.Constantes.aleatorio;

class GestorPrestamos implements Runnable {
    private static final int MIN_PRESTAMOS = 3;
    private static final int MAX_PRESTAMOS = 6;
    private static final int NUM_GENERADORES = 2;
    private static final int MIN_TIEMPO = 2;
    private static final int MAX_TIEMPO = 6;

    private final String iD;
    private final Queue<Usuario> listaUsuarios;
    private final Queue<Libro> listaPrestamos;
    private final List<Libro> resultados;
    private final Lock exm;
    private final CyclicBarrier sincFinalizacion;
    private final CyclicBarrier sincGeneracion;

    public GestorPrestamos(String iD, Queue<Usuario> listaUsuarios, Queue<Libro> listaPrestamos,
                           List<Libro> resultados, Lock exm, CyclicBarrier sincFinalizacion) {
        this.iD = iD;
        this.listaUsuarios = listaUsuarios;
        this.listaPrestamos = listaPrestamos;
        this.resultados = resultados;
        this.exm = exm;
        this.sincFinalizacion = sincFinalizacion;
        this.sincGeneracion = new CyclicBarrier(NUM_GENERADORES + 1);
    }

    public String getiD() {
        return iD;
    }

    @Override
    public void run() {
        System.out.println("Hilo("+iD+") comienza su ejecución");
        // Inicialización del gestor
        int numPrestamos = aleatorio.nextInt(MIN_PRESTAMOS,MAX_PRESTAMOS);
        // Crea y ejecuta sus generadores de préstamos asociados
        Thread[] generadores;
        generadores = new Thread[NUM_GENERADORES];
        Arrays.setAll(generadores,
                i -> generadores[i] = new Thread(new GeneradorDePrestamos(iD+"-Generador("+i+")",
                        listaPrestamos,sincGeneracion)));
        Arrays.stream(generadores).forEach(Thread::start);

        try {
            while ( numPrestamos > 0 ) {
                Optional<Usuario> usuario = obtenerUsuario();
                if ( usuario.isPresent() ) {
                    numPrestamos -= asignarPrestamo(usuario.get());
                    comprobarDevoluciones(usuario.get());
                }
            }

            sincFinalizacion.await(); // Indica al hilo principal que ha finalizado
            System.out.println("Hilo("+iD+") ha FINALIZADO");
        } catch (InterruptedException | BrokenBarrierException e) {
            System.out.println("Hilo("+iD+") ha sido INTERRUMPIDO");
        } finally {
            finalizacion(generadores);
        }
    }

    /**
     * Comprobamos si el usuario tiene devoluciones pendientes de préstamo para añadirlos
     * al resultado del gestor antes de devolver al usuario a la lista compartida de usuarios
     * que tienen los gestores.
     * @param usuario usuaro al que se comprueban sus devoluciones
     */
    private void comprobarDevoluciones(Usuario usuario) {
        List<Libro> devoluciones;

        devoluciones = usuario.devolucionPrestamo();
        devoluciones.forEach(resultados::add);
        exm.lock();
        try {
            listaUsuarios.add(usuario);
        } finally {
            exm.unlock();
        }
    }

    /**
     * Obtenemos el primer usuario disponible de la lista compartida de usuarios para
     * realizar el préstamo
     * @return un usuario que esté presente en la lista compartida
     * @throws InterruptedException
     */
    private Optional<Usuario> obtenerUsuario() {
        Optional<Usuario> resultado = Optional.empty();
        exm.lock();
        try {
            if ( !listaUsuarios.isEmpty() )
                resultado = Optional.of(listaUsuarios.poll());
        } finally {
            exm.unlock();
        }

        return resultado;
    }

    /**
     * Asigna al usuario un préstamo de libros atendiendo a su disponibilidad actual. Si no se disponen
     * de suficientes libros para el préstamo se solicitará a los generadores asociados que creen más.
     * @param usuario usuario al que se le realizará el préstamo
     * @throws BrokenBarrierException
     * @throws InterruptedException
     */
    private int asignarPrestamo(Usuario usuario) throws BrokenBarrierException, InterruptedException {
        int numLibros = 0;
        
        if ( usuario.getDisponibilidad() > 0 ) {
            numLibros = aleatorio.nextInt(usuario.getDisponibilidad()) + 1;
            System.out.println("Hilo(" + iD + ") asigna al usuario " + usuario.getiD() + " " + numLibros
                    + " libros en préstamo");
            while (listaPrestamos.size() < numLibros)
                solicitarPrestamos();

            for (int i = 0; i < numLibros; i++) {
                Libro prestamo = listaPrestamos.poll();
                prestamo.setFechaPrestamo(new Date());
                usuario.asignarPrestamo(prestamo);
                resultados.add(prestamo);
            }

            int tiempo = aleatorio.nextInt(MIN_TIEMPO, MAX_TIEMPO);
            TimeUnit.SECONDS.sleep(tiempo);
        }

        return numLibros;
    }

    /**
     * Solicita a sus generadores asociados que generen nuevos préstamos y espera a que hayan
     * finalizado la operación
     * @throws BrokenBarrierException
     * @throws InterruptedException
     */
    private void solicitarPrestamos() throws BrokenBarrierException, InterruptedException {
        System.out.println("Hilo("+iD+") solicita a sus generadores nuevos préstamos");
        sincGeneracion.await(); // Solicita unos nuevos préstamos
        sincGeneracion.reset();
        System.out.println("Hilo("+iD+") espera ha que finalice la generación de préstamos");
        sincGeneracion.await();
        sincGeneracion.reset();
        System.out.println("Hilo("+iD+") se ha completado la generación de préstamos");
    }

    /**
     * Tiene que esperar a la finalización de los generadores antes de poder
     * ser interrumpido o a su finalización normal.
     * @param generadores
     */
    private void finalizacion(Thread[] generadores) {
        Arrays.stream(generadores).forEach(Thread::interrupt);
        try {
            for (Thread thread : generadores) {
                thread.join();
            }
        } catch (InterruptedException e) {
            // No hacemos nada porque se está finalizando
        }
    }
}
