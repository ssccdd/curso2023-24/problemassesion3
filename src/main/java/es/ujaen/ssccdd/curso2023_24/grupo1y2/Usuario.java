package es.ujaen.ssccdd.curso2023_24.grupo1y2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static es.ujaen.ssccdd.curso2023_24.grupo1y2.Constantes.*;

public class Usuario {
    private final String iD;
    private final TipoUsuario tipoUsuario;
    private final int tiempoPrestamo;
    private final List<Libro> librosEnPrestamo;

    public Usuario(String iD, TipoUsuario tipoUsuario, int tiempoPrestamo) {
        this.iD = iD;
        this.tipoUsuario = tipoUsuario;
        this.tiempoPrestamo = tiempoPrestamo;
        this.librosEnPrestamo = new ArrayList<>();
    }

    public String getiD() {
        return iD;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    /**
     * Se intenta asignar los libros del prestamo al usuario
     * @param prestamo lista de libros que se le prestan al usuario
     * @return lista de libros que no se han podido prestar
     */
    public List<Libro> asignarPrestamo(Libro ... prestamo) {
        List<Libro> resultado = new ArrayList<>();

        Arrays.stream(prestamo).forEach(libro -> {
            if (librosEnPrestamo.size() < tipoUsuario.getMaxLibros())
                librosEnPrestamo.add(libro);
            else
                resultado.add(libro);
        });

        return resultado;
    }

    /**
     * Comprueba los libros que ya han cumplido su plazo de entrega para ser
     * devueltos.
     * @return la lista de libros que han cumplido su plazo de préstamo
     */
    public List<Libro> devolucionPrestamo() {
        List<Libro> resultado = new ArrayList<>();
        Iterator<Libro> it = librosEnPrestamo.iterator();

        while( it.hasNext() ) {
            Libro libro = it.next();
            if( vencimiento.test(sumarSegundos.apply(libro.getFechaPrestamo(),tiempoPrestamo)) ) {
                resultado.add(libro);
                it.remove();
            }
        }

        return resultado;
    }

    /**
     * Nos indica la capacidad de préstamo que tiene el usuario
     * @return número de libos que puede solicitar en préstamo
     */
    public int getDisponibilidad() {
        return tipoUsuario.getMaxLibros() - librosEnPrestamo.size();
    }
    @Override
    public String toString() {
        String resultado = "Usuario{";

        resultado += "\n\tiD: " + iD +
                     "\n\tTipo Usuario: " + tipoUsuario +
                     "\n\tTiempo de préstamo: " + tiempoPrestamo +
                     "\n\tLibros en préstamo: " + librosEnPrestamo + "\n}";

        return resultado;
    }
}
