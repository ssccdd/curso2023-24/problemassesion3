package es.ujaen.ssccdd.curso2023_24.grupo1y2;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.Predicate;

public interface Constantes {
    int MAX_LIBROS_DEVOLUCION = 3;
    int MAX_TIEMPO_DEVOLUCION = 6;
    int MIN_TIEMPO = 2;

    /**
     * Suma una cantidad de segundos a una fecha dada y nos devuelve la nueva
     * fecha.
     */
    BiFunction<Date, Integer, Date> sumarSegundos = (fecha, segundos) -> {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.SECOND, segundos);
        return calendar.getTime();
    };

    /**
     * Predicado para comprobar si se ha alcanzado el vencimiento de una fecha
     * comparando con la fecha actual.
     */
    Predicate<Date> vencimiento = (fecha) -> fecha.before(new Date());

    Random aleatorio = new Random();

    enum TipoUsuario {
        ESTANDAR(0,3), PREMIUM(1,5);

        private final int valor;
        private final int maxLibros;

        TipoUsuario(int valor, int maxLibros) {
            this.valor = valor;
            this.maxLibros = maxLibros;
        }

        public static TipoUsuario getTipoUsuario() {
            return  aleatorio.nextInt(2) == 0 ? ESTANDAR : PREMIUM;
        }

        public int getMaxLibros() {
            return maxLibros;
        }
    }

    TipoUsuario[] tipoUsuarios = TipoUsuario.values();

}
