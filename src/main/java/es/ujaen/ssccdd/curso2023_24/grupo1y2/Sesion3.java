package es.ujaen.ssccdd.curso2023_24.grupo1y2;

import java.util.*;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import es.ujaen.ssccdd.curso2023_24.grupo1y2.Constantes.TipoUsuario;

import static es.ujaen.ssccdd.curso2023_24.grupo1y2.Constantes.*;

public class Sesion3 {
    private static final int NUM_GESTORES = 4;
    private static final int FIN_GESTORES = 1;
    private static final int NUM_USUARIOS = 5;

    public static void main(String[] args) throws BrokenBarrierException, InterruptedException {
        // Declaración de variables
        Thread[] listaTareas;
        Queue<Usuario> listaUsuarios;
        Queue<Libro>[] listaPrestamos;
        List<Libro>[] resultadosGestores;
        Lock exm;
        CyclicBarrier finalizacion;

        // Inicialización de variables
        listaTareas = new Thread[NUM_GESTORES];
        listaPrestamos = new Queue[NUM_GESTORES];
        resultadosGestores = new List[NUM_GESTORES];
        listaUsuarios = new ArrayDeque<>();
        exm = new ReentrantLock();
        finalizacion = new CyclicBarrier(FIN_GESTORES + 1);
        for (int i = 0; i < NUM_GESTORES; i++) {
            listaPrestamos[i] = new ArrayDeque<>();
            resultadosGestores[i] = new ArrayList<>();
        }

        // Cuerpo de ejecución
        System.out.println("Hilo(Principal) Comienza su ejecución para");
        for (int i = 0; i < NUM_USUARIOS; i++) {
            TipoUsuario tipoUsuario = TipoUsuario.getTipoUsuario();
            int tiempo = aleatorio.nextInt(MIN_TIEMPO, MAX_TIEMPO_DEVOLUCION);
            Usuario usuario = new Usuario("Usuario("+i+")", tipoUsuario, tiempo);
            listaUsuarios.add(usuario);
        }

        for (int i = 0; i < NUM_GESTORES; i++) {
            GestorPrestamos gestor = new GestorPrestamos("Gestor("+i+")",listaUsuarios,listaPrestamos[i],
                                            resultadosGestores[i],exm,finalizacion);
            listaTareas[i] = new Thread(gestor, gestor.getiD());
        }
        Arrays.stream(listaTareas).forEach(Thread::start);

        System.out.println("Hilo(Principal) Espera a que finalicen " + FIN_GESTORES + " gestores");
        finalizacion.await();
        Arrays.stream(listaTareas).forEach(Thread::interrupt);
        for (Thread listaTarea : listaTareas) {
            listaTarea.join();
        }
        // Presentar resultados
        System.out.println("\n---------- RESULTADOS ----------\n");
        for (int i = 0; i < NUM_GESTORES; i++) {
            System.out.println("********** " + listaTareas[i].getName() + " **********");
            System.out.println("\t Resultado " + resultadosGestores[i]);
        }
        System.out.println("\n----------- FIN RESULTADOS ----------\n");

        // Finalización
        System.out.println("Hilo(Principal) Ha finalizado");
    }
}