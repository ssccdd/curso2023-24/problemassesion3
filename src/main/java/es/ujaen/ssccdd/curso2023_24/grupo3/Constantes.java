package es.ujaen.ssccdd.curso2023_24.grupo3;


import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.Predicate;

public interface Constantes {
    // Generador aleatorio
    Random aleatorio = new Random();


    /**
     * Suma una cantidad de segundos a una fecha dada y nos devuelve la nueva
     * fecha.
     */
    BiFunction<Date, Integer, Date> sumarSegundos = (fecha, segundos) -> {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.SECOND, segundos);
        return calendar.getTime();
    };

    /**
     * Predicado para comprobar si se ha alcanzado el vencimiento de una fecha
     * comparando con la fecha actual.
     */
    Predicate<Date> vencimiento = (fecha) -> fecha.before(new Date());

    enum EstadoBicicleta {
        DISPONIBLE(50,0), ALQUILADA(70,4), EN_REPARACION(85,2),
        EN_TRANSITO(95,6), FUERA_DE_SERVICIO(100,0);

        private final int peso;
        private final int tiempoOperacion;

        EstadoBicicleta(int peso, int tiempoOperacion) {
            this.peso = peso;
            this.tiempoOperacion = tiempoOperacion;
        }

        /**
         * Nos devuelve un estado de la bicicleta de forma aleatoria
         * según el peso asignado a cada etiqueta
         * @return
         */
        public static EstadoBicicleta getEstado() {
            EstadoBicicleta resultado = null;
            int peso = aleatorio.nextInt(D100);
            int indice = 0;

            while( (indice < estadosDisponibles.length) && (resultado == null) ) {
                if( estadosDisponibles[indice].peso > peso )
                    resultado = estadosDisponibles[indice];

                indice++;
            }

            return resultado;
        }

        /**
         * Nos devuelve el tiempo en el que estará en este estado
         * @return El tiempo máximo para este estado
         */
        public int getTiempoOperacion() {
            return tiempoOperacion;
        }
    }

    enum EstadoViaje {CREADO, EN_DESARROLLO, FINALIZADO}

    int D100 = 100; // Para la generación aleatoria del estado de la bicicleta
    EstadoBicicleta[] estadosDisponibles = EstadoBicicleta.values();
    int MINIMO = 2;
    int TIEMPO_ETAPA = 1;
    int NECESITA_REPARACION = 5; // Un 5% de las bicicletas necesitan reparación
}
