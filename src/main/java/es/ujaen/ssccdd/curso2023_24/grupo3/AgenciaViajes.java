package es.ujaen.ssccdd.curso2023_24.grupo3;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Phaser;
import java.util.concurrent.locks.Lock;
import static es.ujaen.ssccdd.curso2023_24.grupo3.Constantes.*;
import static es.ujaen.ssccdd.curso2023_24.grupo3.Constantes.EstadoViaje.EN_DESARROLLO;
import static es.ujaen.ssccdd.curso2023_24.grupo3.Constantes.EstadoViaje.FINALIZADO;
import static es.ujaen.ssccdd.curso2023_24.grupo3.Constantes.EstadoBicicleta.EN_TRANSITO;

public class AgenciaViajes implements Runnable {
    private static final int MIN_VIAJES = 3;
    private static final int MAX_VIAJES = 6;
    private static final int MIN_PARADAS = 3;
    private static final int MAX_PARADAS = EN_TRANSITO.getTiempoOperacion();
    private static final int MIN_BICICLETAS_VIAJE = 4;
    private static final int MIN_BICICLETAS = 7;
    private static final int MAX_BICICLETAS = 10;
    private static final int FINALIZACION = 0;
    private final String iD;
    private final List<Viaje> listaViajes;
    private final Lock exm;
    private Phaser sincViaje;
    private EstacionBicicletas estacion;
    private Viaje viaje;

    public AgenciaViajes(String iD, List<Viaje> listaViajes, Lock exm) {
        this.iD = iD;
        this.listaViajes = listaViajes;
        this.exm = exm;
    }

    public String getiD() {
        return iD;
    }

    @Override
    public void run() {
        int numViajes = aleatorio.nextInt(MIN_VIAJES,MAX_VIAJES);

        System.out.println("Hilo("+iD+") comienza su ejecución");
        inicializacion();

        try {
            while ( numViajes > 0 ) {
                if ( crearViaje(numViajes) ) {
                    iniciaViaje();
                    completarViaje();
                    numViajes--;
                } else
                    numViajes = FINALIZACION;
            }

            System.out.println("Hilo("+iD+") ha FINALIZADO");
        } catch (InterruptedException e) {
            System.out.println("Hilo("+iD+") ha sido INTERRUMPIDO");
        }
    }

    /**
     * Inicializa la estación de bicicletas de la agencia de viajes
     */
    private void inicializacion() {
        int numBicicletas = aleatorio.nextInt(MIN_BICICLETAS,MAX_BICICLETAS);
        Bicicleta[] bicicletasEstacion = new Bicicleta[numBicicletas];
        Arrays.setAll(bicicletasEstacion, i -> new Bicicleta(iD + "-Bicicleta-" + i));
        estacion = new EstacionBicicletas(iD+"-EstacionBicicletas",bicicletasEstacion);
    }

    /**
     * Crea un viaje si hay disponibles bicicletas para hacerlo
     * @param numViaje el viaje que se crea
     * @return true si se ha creado un nuevo viaje, false en otro caso
     */
    private boolean crearViaje(int numViaje) {
        boolean nuevoViaje;
        int bicicletasDisponibles = Math.max(0, estacion.getDisponibles());
        nuevoViaje = bicicletasDisponibles >= MIN_BICICLETAS_VIAJE;

        if ( nuevoViaje ) {
            int numViajeros = aleatorio.nextInt(MINIMO,bicicletasDisponibles);
            int numParadas = aleatorio.nextInt(MIN_PARADAS,MAX_PARADAS);
            sincViaje = new Phaser(numViajeros + 1);
            viaje = new Viaje(iD+"-Viaje-"+numViaje, estacion,numViajeros,numParadas,sincViaje);
            exm.lock();
            try {
                System.out.println("Hilo("+iD+") crea el viaje " + viaje.getiD() + " para "
                        + numViajeros + " viajeros con " + numParadas + " paradas");
                listaViajes.add(viaje);
            } finally {
                exm.unlock();
            }
        }

        return nuevoViaje;
    }

    /**
     * Hasta que todos los implicados en el viaje no están disponibles no comienza
     * @throws InterruptedException
     */
    private void iniciaViaje() throws InterruptedException {
        System.out.println("Hilo("+iD+") espera a que estén todos los viajeros");
        sincViaje.awaitAdvanceInterruptibly(sincViaje.arrive());
        viaje.setEstado(EN_DESARROLLO);
    }

    /**
     * Se sincronizan todas las etapas del viaje con los usuarios que no tienen problemas con su bicicleta
     * @throws InterruptedException
     */
    private void completarViaje() throws InterruptedException {
        for (int i = 0; i < viaje.getNumParadas(); i++) {
            System.out.println("Hilo("+iD+") el viaje " + viaje.getiD() + " está en su etapa " + i);
            sincViaje.awaitAdvanceInterruptibly(sincViaje.arrive());
        }
        viaje.setEstado(FINALIZADO);
    }
}
