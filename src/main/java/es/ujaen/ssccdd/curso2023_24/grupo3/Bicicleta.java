package es.ujaen.ssccdd.curso2023_24.grupo3;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

import static es.ujaen.ssccdd.curso2023_24.grupo3.Constantes.EstadoBicicleta;
import static es.ujaen.ssccdd.curso2023_24.grupo3.Constantes.EstadoBicicleta.DISPONIBLE;

public class Bicicleta implements Comparable<Bicicleta> {
    private final String iD;
    private EstadoBicicleta estado;
    private Date fechaEstado;

    public Bicicleta(String iD) {
        this.iD = iD;
        this.estado = DISPONIBLE;
        this.fechaEstado = new Date();
    }

    public String getiD() {
        return iD;
    }

    public void setEstado(EstadoBicicleta estado) {
        this.estado = estado;
    }

    public EstadoBicicleta getEstado() {
        return estado;
    }

    public Date getFechaEstado() {
        return fechaEstado;
    }

    public void setFechaEstado(Date fechaEstado) {
        this.fechaEstado = fechaEstado;
    }

    @Override
    public String toString() {
        return "Bicicleta{" +
                "iD='" + iD + '\'' +
                ", Estado=" + estado +
                ", Ultimo Cambio de Estado=" + fechaEstado +
                '}';
    }

    @Override
    public int compareTo(@NotNull Bicicleta bicicleta) {
        return this.iD.compareTo(bicicleta.getiD());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bicicleta bicicleta = (Bicicleta) o;

        return iD.equals(bicicleta.iD);
    }

    @Override
    public int hashCode() {
        return iD.hashCode();
    }
}
