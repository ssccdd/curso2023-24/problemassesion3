package es.ujaen.ssccdd.curso2023_24.grupo3;

import org.jetbrains.annotations.NotNull;

import java.util.*;

import static es.ujaen.ssccdd.curso2023_24.grupo3.Constantes.*;
import static es.ujaen.ssccdd.curso2023_24.grupo3.Constantes.EstadoBicicleta.*;

public class EstacionBicicletas {
    private static final int PRIMERO = 0;
    private final String iD;
    private final List<Bicicleta>[] bicicletasAsignadas;
    private int numEstropeadas;
    private int numReparadas;
    private int numFueraPlazo;
    private int numNoRecogidas;
    private int numNoDisponible;

    public EstacionBicicletas(String iD, Bicicleta ... bicicletas) {
        this.iD = iD;
        this.numNoDisponible = 0;
        this.numEstropeadas = 0;
        this.numFueraPlazo = 0;
        this.numNoRecogidas = 0;
        this.numReparadas = 0;

        // Inicializar bicicletas en la estación
        this.bicicletasAsignadas = new List[estadosDisponibles.length];
        Arrays.setAll(bicicletasAsignadas, i -> new ArrayList<>());
        bicicletasAsignadas[DISPONIBLE.ordinal()] = new ArrayList<>(Arrays.stream(bicicletas).toList());
    }

    public String getiD() {
        return iD;
    }

    public int getDisponibles() {
        return bicicletasAsignadas[DISPONIBLE.ordinal()].size();
    }

    /**
     * Bicicletas que tiene la estación en un estado determinado
     * @param estado es el estado por el que se le pregunta a la estación
     * @return número de bicicletas en ese estado
     */
    public int getNumBicicletas( EstadoBicicleta estado ) {
        return bicicletasAsignadas[estado.ordinal()].size();
    }

    /**
     * Cambia el estado de una bicicleta de DISPONIBLE a ALQUILADA si es posible
     * @return la bicicleta alquilada si hay disponibilidad
     */
    public Optional<Bicicleta> alquilarBicicleta() {
        Optional<Bicicleta> resultado = Optional.empty();
        Bicicleta bicicleta;

        if( !bicicletasAsignadas[DISPONIBLE.ordinal()].isEmpty() ) {
            bicicleta = bicicletasAsignadas[DISPONIBLE.ordinal()].remove(PRIMERO);
            bicicleta.setEstado(ALQUILADA);
            bicicleta.setFechaEstado(new Date());
            bicicletasAsignadas[ALQUILADA.ordinal()].add(bicicleta);
            resultado = Optional.of(bicicleta);
        } else
            numNoDisponible++;

        return resultado;
    }

    /**
     * Se comprueba que haya una bicicleta lista para recoger con el estado ALQUILADA
     * @return la bicicleta recogida si había disponible
     */
    public Optional<Bicicleta> recogerBicicleta() {
        Optional<Bicicleta> resultado = Optional.empty();
        Bicicleta bicicleta;

        if( !bicicletasAsignadas[ALQUILADA.ordinal()].isEmpty() ) {
            bicicleta = bicicletasAsignadas[ALQUILADA.ordinal()].remove(PRIMERO);
            if ( vencimiento.test(sumarSegundos.apply(bicicleta.getFechaEstado(),ALQUILADA.getTiempoOperacion())) )
                // No se ha recogido en plazo
                numFueraPlazo++;

            bicicleta.setEstado(EN_TRANSITO);
            bicicleta.setFechaEstado(new Date());
            bicicletasAsignadas[EN_TRANSITO.ordinal()].add(bicicleta);
            resultado = Optional.of(bicicleta);
        } else
            // No se ha atendido una petición de alquiler
            numNoRecogidas++;

        return resultado;
    }

    /**
     * Llegan a la estación un conjunto de bicicletas. Algunas pueden tener que ser reparadas
     * @param bicicletasEntregadas lista de bicicletas
     */
    public void devolverBicicleta(@NotNull List<Bicicleta> bicicletasEntregadas) {
        for( Bicicleta bicicleta : bicicletasEntregadas ) {
            guardarEnEstacion(bicicleta);
        }
    }

    /**
     * Se entrega una bicicleta a la estación y se comprueba si ha tenido alguna incidencia
     * @param bicicleta la bicicleta que se entrega
     */
    public void devolverBicicleta(@NotNull Bicicleta bicicleta ) {
        guardarEnEstacion(bicicleta);
    }

    private void guardarEnEstacion(@NotNull Bicicleta bicicleta) {
        if ( vencimiento.test(sumarSegundos.apply(bicicleta.getFechaEstado(),EN_TRANSITO.getTiempoOperacion())))
            // No se ha devuelto en plazo
            numFueraPlazo++;

        bicicletasAsignadas[EN_TRANSITO.ordinal()].remove(bicicleta);

        bicicleta.setFechaEstado(new Date());
        if( FUERA_DE_SERVICIO.equals(bicicleta.getEstado()) ) {
            bicicletasAsignadas[FUERA_DE_SERVICIO.ordinal()].add(bicicleta);
            numEstropeadas++;
        } else {
            bicicleta.setEstado(DISPONIBLE);
            bicicletasAsignadas[DISPONIBLE.ordinal()].add(bicicleta);
        }
    }

    /**
     * Las bicicletas FUERA_DE_SERVICIO se repararán. El estado quedará limpio
     * @return la lista de bicicletas FUERA_DE_SERVICIO
     */
    public List<Bicicleta> repararBicicletas() {
        List<Bicicleta> resultado;

        resultado = new ArrayList<>(bicicletasAsignadas[FUERA_DE_SERVICIO.ordinal()]);
        for (Bicicleta bicicleta : resultado ) {
            bicicleta.setEstado(EN_REPARACION);
            bicicleta.setFechaEstado(new Date());
            bicicletasAsignadas[EN_REPARACION.ordinal()].add(bicicleta);
        }

        bicicletasAsignadas[FUERA_DE_SERVICIO.ordinal()].clear();

        return resultado;
    }

    /**
     * Se asignan a DISPONIBLE las bicicletas reparadas. Se comprueba que todas se han reparado en el tiempo
     * establecido
     * @param bicicletasReparadas bicicletas que se añadirán al estado de DISPONIBLE
     */
    public void bicicletasReparadas(@NotNull List<Bicicleta> bicicletasReparadas) {
        numReparadas += bicicletasReparadas.size();
        for( Bicicleta bicicleta : bicicletasReparadas ) {
            if( vencimiento.test(sumarSegundos.apply(bicicleta.getFechaEstado(),EN_REPARACION.getTiempoOperacion())) )
                // No se ha reparado en plazo
                numFueraPlazo++;

            bicicletasAsignadas[EN_REPARACION.ordinal()].remove(bicicleta);

            bicicleta.setEstado(DISPONIBLE);
            bicicleta.setFechaEstado(new Date());
            bicicletasAsignadas[DISPONIBLE.ordinal()].add(bicicleta);
        }
    }

    @Override
    public String toString() {
        String resultado = "EstacionBicicletas{\n\t";
        resultado += "ID: " + iD + "\n\tPlazas de la Estación: \n";

        for ( EstadoBicicleta estado : estadosDisponibles ) {
            resultado += "\t   " + estado + " : " + bicicletasAsignadas[estado.ordinal()] + "\n";
        }

        resultado += "\tOperaciones fuera de plazo:\t" + numFueraPlazo +
                     "\n\tAlquileres no atendidos:\t" + numNoDisponible +
                     "\n\tRecogidas no atendidas:\t\t" + numNoRecogidas +
                     "\n\tBicicletas estropeadas:\t\t" + numEstropeadas +
                     "\n\tBicicletas reparadas:\t\t" + numReparadas + "\n}";

        return resultado;
    }
}
