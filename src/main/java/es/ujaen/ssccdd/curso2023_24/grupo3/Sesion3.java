package es.ujaen.ssccdd.curso2023_24.grupo3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static es.ujaen.ssccdd.curso2023_24.grupo3.Constantes.aleatorio;

public class Sesion3 {
    private static final int MIN_AGENCIAS = 2;
    private static final int MAX_AGENCIAS = 5;
    private static final int MIN_USUARIOS = 10;
    private static final int MAX_USUARIOS = 15;
    private static int TIEMPO_EJECUCION = 10; // segundos

    public static void main(String[] args) throws InterruptedException {
        // Declaración de variables
        Thread[] listaTareas;
        List<Viaje> listaViajes;
        Lock exm;
        int numAgencias;
        int numUsuarios;

        // Inicialización de variables
        numAgencias = aleatorio.nextInt(MIN_AGENCIAS,MAX_AGENCIAS);
        numUsuarios = aleatorio.nextInt(MIN_USUARIOS,MAX_USUARIOS);
        listaTareas = new Thread[numAgencias+numUsuarios];
        listaViajes = new ArrayList<>();
        exm = new ReentrantLock();
        for (int i = 0; i < numAgencias; i++) {
            AgenciaViajes agencia = new AgenciaViajes("Agencia("+i+")",listaViajes,exm);
            listaTareas[i] = new Thread(agencia, agencia.getiD());
        }
        for (int i = numAgencias; i < listaTareas.length; i++) {
            Usuario usuario = new Usuario("Usuario("+i+")",listaViajes,exm);
            listaTareas[i] = new Thread(usuario, usuario.getiD());
        }

        // Cuerpo de ejecución
        System.out.println("Hilo(Principal) Comienza su ejecución para");
        Arrays.stream(listaTareas).forEach(Thread::start);

        TimeUnit.SECONDS.sleep(TIEMPO_EJECUCION);

        Arrays.stream(listaTareas).forEach(Thread::interrupt);
        for (Thread tarea : listaTareas)
            tarea.join();

        // Presentar resultados
        System.out.println("\n---------- RESULTADOS ----------\n");
        listaViajes.forEach(viaje ->
                System.out.println("********** " + viaje.getiD() + " **********\n\t"+viaje));
        System.out.println("\n----------- FIN RESULTADOS ----------\n");

        // Finalización
        System.out.println("Hilo(Principal) Ha finalizado");
    }
}