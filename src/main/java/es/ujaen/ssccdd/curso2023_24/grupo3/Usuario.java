package es.ujaen.ssccdd.curso2023_24.grupo3;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

import static es.ujaen.ssccdd.curso2023_24.grupo3.Constantes.*;
import static es.ujaen.ssccdd.curso2023_24.grupo3.Constantes.EstadoBicicleta.FUERA_DE_SERVICIO;

public class Usuario implements Runnable {
    private static final boolean INCIDENCIA = true;
    private static final int INICIO = 0;
    private final String iD;
    private final List<Viaje> listaViajes;
    private final Lock exm;
    private Viaje viaje;
    private Optional<Phaser> sincViaje;
    private Bicicleta bicicleta;

    public Usuario(String iD, List<Viaje> listaViajes, Lock exm) {
        this.iD = iD;
        this.listaViajes = listaViajes;
        this.exm = exm;
    }

    public String getiD() {
        return iD;
    }

    @Override
    public void run() {
        System.out.println("Hilo("+iD+") comienza su ejecución");

        try {
            while (true) {
                apuntarseViaje();
                if ( sincViaje.isPresent() ) {
                    if ( iniciarViaje(sincViaje.get()) )
                        completarViaje(sincViaje.get());
                }
            }
        } catch (InterruptedException e) {
            System.out.println("Hilo("+iD+") ha FINALIZADO");
        }
    }

    /**
     * Busca el primer viaje donde pueda apuntarse el usuario
     * @throws InterruptedException
     */
    private void apuntarseViaje() throws InterruptedException {
        sincViaje = Optional.empty();

        exm.lockInterruptibly();
        try {
            Iterator<Viaje> it = listaViajes.iterator();

            while ( it.hasNext() && sincViaje.isEmpty() ) {
                viaje = it.next();
                sincViaje = viaje.apuntarseViaje();
            }
        } finally {
            exm.unlock();
        }
    }

    /**
     * Recoge la bicicleta para iniciar el viaje y espera a que todos los usuarios estén listos.
     * Puede que no llegue a tiempo para recoger la bicicleta y no podrá iniciar el viaje.
     * @param sincViaje el elemento de sincronización para el viaje
     * @throws InterruptedException
     */
    private boolean iniciarViaje(Phaser sincViaje) throws InterruptedException {
        Optional<Bicicleta> recogida = viaje.recogerBicicleta(iD);

        if ( recogida.isPresent() ) {
            System.out.println("Hilo(" + iD + ") listo para comenzar el viaje " + viaje.getiD());
            bicicleta = recogida.get();
            sincViaje.awaitAdvanceInterruptibly(sincViaje.arrive());
        } else {
            System.out.println("Hilo(" + iD + ") no ha recogido a tiempo la bicicleta para iniciar el " + viaje.getiD());
            sincViaje.arriveAndDeregister();
        }

        return recogida.isPresent();
    }

    /**
     * Se van completando las diferentes etapas del viaje sincronizandose cada una de ellas
     * con todos los viajeros que no sufren problemas.
     * @param sincViaje elemento de sincronización para el viaje
     * @throws InterruptedException
     */
    private void completarViaje(Phaser sincViaje) throws InterruptedException {
        boolean incidencia = !INCIDENCIA;
        int etapa = INICIO;

        while ( etapa < viaje.getNumParadas() && !incidencia ) {
            System.out.println("Hilo("+iD+") el viaje " + viaje.getiD() + " está en su etapa " + etapa);
            TimeUnit.SECONDS.sleep(TIEMPO_ETAPA);

            int posibleIncidencia = aleatorio.nextInt(D100);
            if ( posibleIncidencia < NECESITA_REPARACION ) {
                incidencia = INCIDENCIA;
                sincViaje.arriveAndDeregister(); // Deja el viaje porque se ha estropeado la bicicleta
                bicicleta.setEstado(FUERA_DE_SERVICIO);
                System.out.println("Hilo("+iD+") el usuario " + iD + " deja el viaje en la etapa " + etapa +
                        " por problema con la bicicleta");
            } else {
                // Espera al resto de viajeros para comenzar la siguiente etapa
                sincViaje.awaitAdvanceInterruptibly(sincViaje.arrive());
            }

            etapa++;
        }

        viaje.devolverBicicleta(bicicleta);
        viaje.registroViaje(iD,etapa);
    }
}
