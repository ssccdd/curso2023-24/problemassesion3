package es.ujaen.ssccdd.curso2023_24.grupo3;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Phaser;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import es.ujaen.ssccdd.curso2023_24.grupo3.Constantes.EstadoViaje;

import static es.ujaen.ssccdd.curso2023_24.grupo3.Constantes.EstadoViaje.CREADO;

public class Viaje {
    private final String iD;
    private final EstacionBicicletas estacion;
    private int numUsuarios;
    private int numParadas;
    private final List<String> registroViaje;
    private final Phaser sincViaje;
    private EstadoViaje estado;
    private Lock exm;

    public Viaje(String iD, EstacionBicicletas estacion, int numUsuarios, int numParadas, Phaser sincViaje) {
        this.iD = iD;
        this.estacion = estacion;
        this.numUsuarios = numUsuarios;
        this.numParadas = numParadas;
        this.sincViaje = sincViaje;
        this.registroViaje = new ArrayList<>();
        this.estado = CREADO;
        this.exm = new ReentrantLock();
    }

    public String getiD() {
        return iD;
    }

    public int getNumParadas() {
        return numParadas;
    }

    public void setEstado(EstadoViaje estado) {
        this.estado = estado;
    }

    /**
     * El usuario que participa en el viaje recoge la bicicleta para ello.
     * @param usuario nombre del usuario que participa en el viaje
     * @return la bicicleta
     */
    public Optional<Bicicleta> recogerBicicleta(String usuario) {
        Optional<Bicicleta> recogida = Optional.empty();

        exm.lock();
        try {
            recogida = estacion.recogerBicicleta();

            if (recogida.isPresent())
                registroViaje.add("Recogida la bicicleta por " + usuario);
            else
                registroViaje.add("No ha llegado a tiempo para iniciar el viaje " + usuario);
        } finally {
            exm.unlock();
        }

        return recogida;
    }

    /**
     * Se devuelve la bicicleta cuando el usuario termina con el viaje
     * @param bicicleta devolución de la bicicleta
     */
    public void devolverBicicleta(Bicicleta bicicleta) {
        exm.lock();
        try {
            estacion.devolverBicicleta(bicicleta);
        } finally {
            exm.unlock();
        }
    }

    /**
     * Añade el nombre del usuario a la lista de participantes del viaje
     * @param nombre del usuario
     */
    public void registroViaje(String nombre, int etapa) {
        exm.lock();
        try {
            if (etapa < numParadas)
                registroViaje.add("El viajero " + nombre + " ha tenido PROBLEMAS en la etapa " + etapa);
            else
                registroViaje.add("El viajero " + nombre + " ha completado con EXITO el viaje");
        } finally {
            exm.unlock();
        }
    }

    /**
     * Comprueba si un nuevo usuario puede apuntarse al viaje, además deja la bicicleta alquilada
     * para su recogida al inicio del viaje.
     * @return el objeto de sincronización para el viaje
     */
    public Optional<Phaser> apuntarseViaje() {
        Optional<Phaser> resultado = Optional.empty();

        exm.lock();
        try {
            if (numUsuarios > 0) {
                numUsuarios--;
                estacion.alquilarBicicleta();
                resultado = Optional.of(sincViaje);
            }
        } finally {
            exm.unlock();
        }

        return resultado;
    }

    @Override
    public String toString() {
        String resultado = "********** VIAJE(" + iD + ") **********";

        resultado += "\n\t Estado " + estado +
                     "\n\t Registro del viaje " + registroViaje +
                     "\n\t Estación Bicicletas " + estacion.getiD() + "\n" + estacion;
        return resultado;
    }
}
