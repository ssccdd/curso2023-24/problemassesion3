package es.ujaen.ssccdd.curso2023_24.grupo4;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.locks.Lock;

import static es.ujaen.ssccdd.curso2023_24.grupo4.Constantes.*;
import static es.ujaen.ssccdd.curso2023_24.grupo4.Constantes.EstadoPartida.EN_DESARROLLO;
import static es.ujaen.ssccdd.curso2023_24.grupo4.Constantes.EstadoPartida.FINALIZADA;

public class PlataformaJuegos implements Runnable {
    private static final int MIN_JUEGOS = 4;
    private static final int MAX_JUEGOS = 8;
    private static final int MIN_COPIAS = 2;
    private static final int MAX_COPIAS = 5;
    private final String iD;
    private final Queue<Partida> listaPartidas;
    private final Lock exm;
    private CyclicBarrier sincPartida;
    private Partida partida;

    public PlataformaJuegos(String iD, Queue<Partida> listaPartidas, Lock exm) {
        this.iD = iD;
        this.listaPartidas = listaPartidas;
        this.exm = exm;
    }

    public String getiD() {
        return iD;
    }

    @Override
    public void run() {
        System.out.println("Hilo("+iD+") comienza su ejecución");

        int numJuegos = aleatorio.nextInt(MIN_JUEGOS,MAX_JUEGOS);

        try {
            for (int i = 0; i < numJuegos; i++) {
                crearPartida(i);
                inicioPartida();
                completarPartida();
            }

            System.out.println("Hilo("+iD+") ha FINALIZADO correctamente");
        } catch (InterruptedException | BrokenBarrierException e) {
            System.out.println("Hilo("+iD+") ha sido INTERRUMPIDO");
        }
    }

    /**
     * Crea una partida y la añade a la lista compartida de partidas con los
     * organizadores de eventos
     * @param i número de partida creada
     */
    private void crearPartida(int i) {
        CategoriaJuego categoria = CategoriaJuego.getCategoria();
        int numCopias = aleatorio.nextInt(MIN_COPIAS,MAX_COPIAS);
        Queue<Juego> copiasJuegos = new ArrayDeque<>();
        for (int j = 0; j < numCopias; j++)
            copiasJuegos.add(new Juego(iD + "-Juego(" + i + "," + j + ")", categoria));

        sincPartida = new CyclicBarrier(copiasJuegos.size()+1);
        partida = new Partida(iD+"-Partida("+i+")", categoria, copiasJuegos, sincPartida);
        exm.lock();
        try {
            listaPartidas.add(partida);
        } finally {
            exm.unlock();
        }
        System.out.println("Hilo("+iD+") ha creado una patida NUEVA " + partida.getiD() + " para " +
                numCopias + " jugadores") ;
    }

    /**
     * Punto de sincronización con los organizadores de eventos que participan en la partida
     * con un jugador para que todos comiencen la partida al mismo tiempo.
     * @throws BrokenBarrierException
     * @throws InterruptedException
     */
    private void inicioPartida() throws BrokenBarrierException, InterruptedException {
        System.out.println("Hilo("+iD+") está esperando al INICIO de la partida " + partida.getiD());
        sincPartida.await();
        partida.setEstado(EN_DESARROLLO);
        sincPartida.reset(); // Reinicia la barrera para la siguiente etapa
    }

    /**
     * Punto de sincronización para saber cuando todos los jugadores que han participado en la partida
     * la han completado y han dejado su resultado.
     */
    private void completarPartida() throws BrokenBarrierException, InterruptedException {
        sincPartida.await();
        partida.setEstado(FINALIZADA);
        System.out.println("Hilo("+iD+") ha FINALIZADO la partida " + partida.getiD());
    }
}
