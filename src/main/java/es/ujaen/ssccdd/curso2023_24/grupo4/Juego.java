package es.ujaen.ssccdd.curso2023_24.grupo4;

import es.ujaen.ssccdd.curso2023_24.grupo4.Constantes.CategoriaJuego;
import es.ujaen.ssccdd.curso2023_24.grupo4.Constantes.EstadoDevolucion;
import org.jetbrains.annotations.NotNull;

import java.util.Date;

import static es.ujaen.ssccdd.curso2023_24.grupo4.Constantes.EstadoDevolucion.EN_PLAZO;

public class Juego implements Comparable<Juego> {
    private final String iD;
    private final CategoriaJuego categoria;
    private EstadoDevolucion estado;
    private Date fechaPrestamo;

    public Juego(String iD, CategoriaJuego categoria) {
        this.iD = iD;
        this.categoria = categoria;
        this.estado = EN_PLAZO;
        this.fechaPrestamo = new Date();
    }

    public String getiD() {
        return iD;
    }

    public CategoriaJuego getCategoria() {
        return categoria;
    }

    public EstadoDevolucion getEstado() {
        return estado;
    }

    public void setEstado(EstadoDevolucion estado) {
        this.estado = estado;
    }

    public Date getFechaPrestamo() {
        return fechaPrestamo;
    }

    public void setFechaPrestamo(Date fechaPrestamo) {
        this.fechaPrestamo = fechaPrestamo;
    }

    @Override
    public String toString() {
        return "Juego{" +
                "iD='" + iD + '\'' +
                ", categoria=" + categoria +
                ", estado='" + estado + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Juego juego = (Juego) o;

        return iD.equals(juego.iD);
    }

    @Override
    public int hashCode() {
        return iD.hashCode();
    }

    @Override
    public int compareTo(@NotNull Juego juego) {
       return this.iD.compareTo(juego.getiD());
    }
}
