package es.ujaen.ssccdd.curso2023_24.grupo4;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.Predicate;

public interface Constantes {
    Random aleatorio = new Random();

    /**
     * Suma una cantidad de segundos a una fecha dada y nos devuelve la nueva
     * fecha.
     */
    BiFunction<Date, Integer, Date> sumarSegundos = (fecha, segundos) -> {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.SECOND, segundos);
        return calendar.getTime();
    };

    /**
     * Predicado para comprobar si se ha alcanzado el vencimiento de una fecha
     * comparando con la fecha actual.
     */
    Predicate<Date> vencimiento = (fecha) -> fecha.before(new Date());

    enum CategoriaJuego {
        ACCION(50,5), AVENTURA(20,5), ESTRATEGIA(15,10), DEPORTES(10,6),
        PUZZLE(5,4);

        private final int peso;
        private final int tiempo;

        CategoriaJuego(int peso, int tiempo) {
            this.peso = peso;
            this.tiempo = tiempo;
        }

        /**
         * Devuelve una categoría de juego aleatoria atendendo a las
         * preferencias del usuario.
         * @return una categoría de juego
         */
        public static CategoriaJuego getCategoria() {
            CategoriaJuego resultado = null;
            int suma = 0;
            int indice = 0;
            int peso = aleatorio.nextInt(D100);

            while( (indice < listaCategorias.length) && (resultado == null) ) {
                suma += listaCategorias[indice].peso;
                if( suma > peso )
                    resultado = listaCategorias[indice];

                indice++;
            }

            return resultado;
        }

        /**
         * Tiempo máximo que tiene el usuario para poder devolver un juego
         * asignado
         * @return el tiempo máximo de asignación
         */
        public int getTiempo() {
            return tiempo;
        }
    }

    enum EstadoDevolucion { EN_PLAZO, FUERA_DE_PLAZO }
    enum EstadoPartida { CREADA, EN_DESARROLLO, FINALIZADA }

    int D100 = 100; // Para una tirada aleatoria de probabilidad
    CategoriaJuego[] listaCategorias = CategoriaJuego.values();
    boolean ASIGNADO = true;
    int TIEMPO_CICLO = 2; // segundos
    int TIEMPO_EJECUCION = 30; // segundos
}
