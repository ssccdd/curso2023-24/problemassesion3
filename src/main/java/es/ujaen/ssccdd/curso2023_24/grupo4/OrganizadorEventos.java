package es.ujaen.ssccdd.curso2023_24.grupo4;

import java.util.*;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

import static es.ujaen.ssccdd.curso2023_24.grupo4.Constantes.aleatorio;

public class OrganizadorEventos implements Runnable {
    private static final int MIN_JUEGOS = 4;
    private static final int MAX_JUEGOS = 8;
    private static final double MIN_TIEMPO = 0.6; // 60%
    private static final double MAX_TIEMPO = 1.2; // 120%
    private static final boolean ENCONTRADO = true;
    private final String iD;
    private final Queue<Partida> listaPartidas;
    private final List<Usuario> listaJugadores;
    private final Lock exmPartidas;
    private final Lock exmJugadores;
    private int numPartidas;
    private Optional<CyclicBarrier> sincPartida;
    private Partida partida;
    private Juego juegoPartida;
    private Usuario jugadorPartida;


    public OrganizadorEventos(String iD, Queue<Partida> listaPartidas, List<Usuario> listaJugadores,
                              Lock exmPartidas, Lock exmJugadores) {
        this.iD = iD;
        this.listaPartidas = listaPartidas;
        this.listaJugadores = listaJugadores;
        this.exmPartidas = exmPartidas;
        this.exmJugadores = exmJugadores;
        this.numPartidas = aleatorio.nextInt(MIN_JUEGOS, MAX_JUEGOS);
        this.sincPartida = Optional.empty();
    }

    public String getiD() {
        return iD;
    }

    @Override
    public void run() {
        System.out.println("Hilo("+iD+") comienza su ejecución");

        try {
            while ( numPartidas > 0 ){
                obtenerPartida();
                if ( sincPartida.isPresent() ) {
                    inicioPartida(sincPartida.get());
                    completarPartida(sincPartida.get());
                }
            }

            System.out.println("Hilo("+iD+") ha FINALIZADO correctamente");
        } catch (InterruptedException | BrokenBarrierException e) {
            System.out.println("Hilo("+iD+") ha sido INTERRUMPIDO");
        }
    }

    /**
     * Recorre la lista de partidas hasta encontrar una en la que se obtiene el elemento
     * de sincronización que permitirá participar en la partida
     * @throws InterruptedException
     */
    private void obtenerPartida() throws InterruptedException {
        sincPartida = Optional.empty();

        exmPartidas.lockInterruptibly();
        try {
            Iterator<Partida> it = listaPartidas.iterator();

            while ( it.hasNext() && sincPartida.isEmpty() ) {
                partida = it.next();
                sincPartida = partida.apuntarsePartida();
            }
        } finally {
            exmPartidas.unlock();
        }
    }

    /**
     * El organizador busca un jugador al que asignar el juego para la partida y espera
     * hasta que el resto de organizadores con sus jugadores estén listos para empezar la partida.
     * También deben esperar a que la plataforma de juegos esté lista para ello.
     * @param sincPartida elemento de sincronización de la partida.
     * @throws BrokenBarrierException
     * @throws InterruptedException
     */
    private void inicioPartida(CyclicBarrier sincPartida) throws BrokenBarrierException, InterruptedException {
        juegoPartida = partida.getJuego();
        System.out.println("Hilo("+iD+") busca jugador para el juego " + juegoPartida + " en la partida "
                + partida.getiD());
        buscarJugador();

        System.out.println("Hilo("+iD+") el jugador " + jugadorPartida.getiD() + " se une a la partida "
                            + partida.getiD());
        partida.enviarJugador(jugadorPartida.getiD());
        sincPartida.await();
    }

    /**
     * Encuentra al primer jugador al que se le puede asignar el juego de la partida para
     * este organizador de eventos
     * @throws InterruptedException
     */
    private void buscarJugador() throws InterruptedException {
        boolean encontradoJugador = !ENCONTRADO;

        while ( !encontradoJugador ) {
            exmJugadores.lockInterruptibly();
            try {
                Iterator<Usuario> it = listaJugadores.iterator();

                while ( it.hasNext() && !encontradoJugador ) {
                    jugadorPartida = it.next();
                    encontradoJugador = jugadorPartida.asignarJuego(juegoPartida);
                }
            } finally {
                exmJugadores.unlock();
            }
        }
    }

    /**
     * Simulará un tiempo de juego del jugador en la partida y devolverá la copia de su juego
     * al finalizar
     * @param sincPartida elemento de sincronización para indicar el fin de la partida
     * @throws InterruptedException
     * @throws BrokenBarrierException
     */
    private void completarPartida(CyclicBarrier sincPartida) throws InterruptedException, BrokenBarrierException {
        int minTiempo = (int) (juegoPartida.getCategoria().getTiempo() * MIN_TIEMPO);
        int maxTiempo = (int) (juegoPartida.getCategoria().getTiempo() * MAX_TIEMPO);
        int tiempoPartida = aleatorio.nextInt(minTiempo,maxTiempo);

        System.out.println("Hilo("+iD+") el " + jugadorPartida.getiD() + " juega por " +
                tiempoPartida + " segundos en " + partida.getiD());
        TimeUnit.SECONDS.sleep(tiempoPartida);

        System.out.println("Hilo("+iD+" el " + jugadorPartida.getiD() + " ha concluido la partida y espera " +
                "al resto de jugadores del " + partida.getiD());
        sincPartida.await();
        jugadorPartida.devolverJuego(new Date(), juegoPartida);
        partida.devolverJuego(juegoPartida);
        numPartidas--;
    }
}
