package es.ujaen.ssccdd.curso2023_24.grupo4;

import es.ujaen.ssccdd.curso2023_24.grupo4.Constantes.CategoriaJuego;
import es.ujaen.ssccdd.curso2023_24.grupo4.Constantes.EstadoPartida;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static es.ujaen.ssccdd.curso2023_24.grupo4.Constantes.EstadoPartida.CREADA;

public class Partida {
    private final String iD;
    private final CategoriaJuego categoria;
    private final Queue<Juego> copiasJuego;
    private final CyclicBarrier sincronizacion;
    private int numJugadores;
    private EstadoPartida estado;
    private final List<String> jugadoresPartida;
    private final Lock exm;

    public Partida(String iD, CategoriaJuego categoria, Queue<Juego> copiasJuego, CyclicBarrier sincronizacion) {
        this.iD = iD;
        this.categoria = categoria;
        this.copiasJuego = copiasJuego;
        this.sincronizacion = sincronizacion;
        this.numJugadores = copiasJuego.size();
        this.jugadoresPartida = new ArrayList<>();
        this.estado = CREADA;
        this.exm = new ReentrantLock();
    }

    public String getiD() {
        return iD;
    }

    public CategoriaJuego getCategoria() {
        return categoria;
    }

    public void setEstado(EstadoPartida estado) {
        this.estado = estado;
    }

    /**
     * Se obtiene una copia del juego
     * @return una de las copias del juego
     */
    public Juego getJuego() {
       return copiasJuego.poll();
    }

    /**
     * Devuelve la copia del juego una vez finalizada la partida.
     * @param juego
     */
    public void devolverJuego(Juego juego) {
        copiasJuego.add(juego);
    }

    /**
     * Un jugador que quiera participar en la partida deberá obtener
     * el objeto de sincronización que permite participar en ella
     * @return
     */
    public Optional<CyclicBarrier> apuntarsePartida() {
        Optional<CyclicBarrier> resultado = Optional.empty();

        exm.lock();
        try {
            if (numJugadores > 0) {
                numJugadores--;
                resultado = Optional.of(sincronizacion);
            }
        } finally {
            exm.unlock();
        }

        return  resultado;
    }

    /**
     * Añade a uno de los jugadores que han participado en la partida
     * @param nombreJugador el nombre del jugador que participa en la partida
     */
    public void enviarJugador(String nombreJugador) {
        jugadoresPartida.add(nombreJugador);
    }

    @Override
    public String toString() {
        String resultado = "********** PARTIDA(" + iD + ") **********";

        resultado += "\n\tCategoría " + categoria + "\tEstado " + estado +
                     "\n\tCopias juegos " + copiasJuego +
                     "\n\tJugadores participantes " + jugadoresPartida;

        return resultado;
    }
}
