package es.ujaen.ssccdd.curso2023_24.grupo4;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static es.ujaen.ssccdd.curso2023_24.grupo4.Constantes.*;
import static es.ujaen.ssccdd.curso2023_24.grupo4.Constantes.EstadoDevolucion.FUERA_DE_PLAZO;

public class Usuario {
    private static final int MIN_JUEGOS_USUARIO = 1;
    private static final int MAX_JUEGOS_USUARIO = 2;
    private final String iD;
    private final int[] juegosPorCategoria;
    private final List<Juego>[] listaJuegos;

    public Usuario(String iD) {
        this.iD = iD;
        this.juegosPorCategoria = new int[listaCategorias.length];
        Arrays.setAll(juegosPorCategoria, i -> juegosPorCategoria[i] = aleatorio.nextInt(MIN_JUEGOS_USUARIO,MAX_JUEGOS_USUARIO));
        this.listaJuegos = new List[listaCategorias.length];
        Arrays.stream(listaCategorias).forEach(categoria -> listaJuegos[categoria.ordinal()] = new ArrayList<>());
    }

    public String getiD() {
        return iD;
    }

    @Override
    public String toString() {
        String resultado = "Usuario{";

        resultado += "\n\tiD='" + iD +
                     "\n\tMáximo de juegos por categoría :" + Arrays.toString(juegosPorCategoria);

        for ( CategoriaJuego categoria : listaCategorias)
            resultado += "\n\t  Categoria: " + listaJuegos[categoria.ordinal()];

        resultado += "\n\t}";

        return resultado;
    }

    /**
     * Se intenta asignar el juego a la lista de juegos del usuario según su categoría si no
     * se ha alcanzado el máximo para esa categoría
     * @param juego que se intenta asignar
     * @return true si se ha asignado el juego o false en otro caso
     */
    public boolean asignarJuego(@NotNull Juego juego) {
        boolean resultado = !ASIGNADO;
        int categoria = juego.getCategoria().ordinal();

        if ( listaJuegos[categoria].size() + 1 <= juegosPorCategoria[categoria] ) {
            juego.setFechaPrestamo(new Date());
            listaJuegos[categoria].add(juego);
            resultado = ASIGNADO;
        }

        return resultado;
    }

    /**
     * Devuelve una lista de juegos atendiendo en un momento de tiempo dado. Se comprueba que se pueda devolver
     * el juego que se pide y se comprueba si se ha devuelto en el tiempo establecido para ello.
     * @param fechaDevolucion momento en el que se va ha realizar la devolución
     * @param listaParaDevolver lista de juegos que se pretende devolver
     * @return la lista de juegos que se han devuelto cambiando el estado si no se han devuelto en plazo
     */
    public List<Juego> devolverJuego(Date fechaDevolucion, Juego ... listaParaDevolver) {
        List<Juego> resultado = new ArrayList<>();

        Arrays.stream(listaParaDevolver).forEach(juego -> {
            CategoriaJuego categoria = juego.getCategoria();
            if (listaJuegos[categoria.ordinal()].remove(juego)) {
                if (vencimiento.test(sumarSegundos.apply(juego.getFechaPrestamo(),categoria.getTiempo())))
                    juego.setEstado(FUERA_DE_PLAZO);
                resultado.add(juego);
            }
        });

        return resultado;
    }

    /**
     * Nos devuelve una lista de los juegos que tiene asignado el usuario y que ya ha cumplido su
     * plazo de entrega
     * @return la lista de juegos que ha cumplido su fecha de entrega
     */
    public List<Juego> juegosCaducados() {
        List<Juego> resultado = new ArrayList<>();

        Arrays.stream(listaCategorias).forEach(categoria -> listaJuegos[categoria.ordinal()].forEach(juego -> {
                if (vencimiento.test(sumarSegundos.apply(juego.getFechaPrestamo(),juego.getCategoria().getTiempo()))) {
                    juego.setEstado(FUERA_DE_PLAZO);
                    resultado.add(juego);
                }
            }
        ));

        return resultado;
    }
}
