package es.ujaen.ssccdd.curso2023_24.grupo4;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static es.ujaen.ssccdd.curso2023_24.grupo4.Constantes.*;
public class Sesion3 {
    private static final int MIN_ORGANIZADORES = 5;
    private static final int MAX_ORGANIZADORES = 10;
    private static final int MIN_PLATAFORMAS = 2;
    private static final int MAX_PLATAFORMAS = 4;
    private static final int NUM_JUGADORES = 5;

    public static void main(String[] args) throws InterruptedException {
        // Declaración de variables
        Thread[] listaTareas;
        Queue<Partida> listaPartidas;
        List<Usuario> listaJugadores;
        Lock exmPartidas;
        Lock exmJugadores;
        int numCiclos;
        int numPlataformas;
        int numOrganizadores;

        // Inicialización de variables
        listaJugadores = new ArrayList<>();
        listaPartidas = new ArrayDeque<>();
        exmJugadores = new ReentrantLock();
        exmPartidas = new ReentrantLock();
        numOrganizadores = aleatorio.nextInt(MIN_ORGANIZADORES,MAX_ORGANIZADORES);
        numPlataformas = aleatorio.nextInt(MIN_PLATAFORMAS,MAX_PLATAFORMAS);
        numCiclos = TIEMPO_EJECUCION / TIEMPO_CICLO;
        listaTareas = new Thread[numOrganizadores+numPlataformas];
        for (int i = 0; i < numOrganizadores; i++) {
            OrganizadorEventos organizador = new OrganizadorEventos("Organizador("+i+")",listaPartidas,
                            listaJugadores,exmPartidas,exmJugadores);
            listaTareas[i] = new Thread(organizador, organizador.getiD());
        }
        for (int i = numOrganizadores; i < listaTareas.length; i++) {
            PlataformaJuegos plataforma = new PlataformaJuegos("Plataforma("+i+")",listaPartidas,exmPartidas);
            listaTareas[i] = new Thread(plataforma, plataforma.getiD());
        }

        // Cuerpo de ejecución
        System.out.println("Hilo(Principal) Comienza su ejecución para");
        Arrays.stream(listaTareas).forEach(Thread::start);

        for (int i = 0; i < numCiclos; i++) {
            int numJugadores = aleatorio.nextInt(NUM_JUGADORES) + 1;
            for (int j = 0; j < numJugadores; j++) {
                Usuario jugador = new Usuario("Jugador("+i+","+j+")");
                exmJugadores.lock();
                try {
                    listaJugadores.add(jugador);
                } finally {
                    exmJugadores.unlock();
                }
            }

            System.out.println("Hilo(Principal) ha creado " + numJugadores + " nuevos jugadores en el ciclo " + i);
            TimeUnit.SECONDS.sleep(TIEMPO_CICLO);
        }

        Arrays.stream(listaTareas).forEach(Thread::interrupt);
        for (Thread tarea : listaTareas)
            tarea.join();

        // Presentar resultados
        System.out.println("\n---------- RESULTADOS ----------\n");
        listaPartidas.forEach(partida ->
                System.out.println("********** " + partida.getiD() + " **********\n\t"+partida));
        System.out.println("\n----------- FIN RESULTADOS ----------\n");

        // Finalización
        System.out.println("Hilo(Principal) Ha finalizado");
    }
}