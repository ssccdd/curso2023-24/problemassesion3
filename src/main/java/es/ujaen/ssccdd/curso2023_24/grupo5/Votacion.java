package es.ujaen.ssccdd.curso2023_24.grupo5;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Phaser;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import es.ujaen.ssccdd.curso2023_24.grupo5.Constantes.EstadoConsenso;

import static es.ujaen.ssccdd.curso2023_24.grupo5.Constantes.EstadoConsenso.PROPUESTA;

public class Votacion {
    private final String iD;
    private final int minNodos;
    private final int umbral;
    private int numModeradores;
    private final Phaser sincronizacion;
    private final List<Decision> resultadoVotacion;
    private EstadoConsenso estadoVotacion;
    private final Lock exm;

    public Votacion(String iD, int minNodos, int umbral, int numModeradores, Phaser sincronizacion) {
        this.iD = iD;
        this.minNodos = minNodos;
        this.umbral = umbral;
        this.numModeradores = numModeradores;
        this.sincronizacion = sincronizacion;
        this.resultadoVotacion = new ArrayList<>();
        this.estadoVotacion = PROPUESTA;
        this.exm = new ReentrantLock();
    }

    public String getiD() {
        return iD;
    }

    public int getMinNodos() {
        return minNodos;
    }

    public int getUmbral() {
        return umbral;
    }

    public EstadoConsenso getEstadoVotacion() {
        return estadoVotacion;
    }

    public void setEstadoVotacion(EstadoConsenso estadoVotacion) {
        this.estadoVotacion = estadoVotacion;
    }

    /**
     * Un moderador que desee participar en la votación deberá intentar
     * unirse y obtener el elemento de sincronización. Si lo obtiene participa
     * de la votación y en otro caso no formará parte de ella.
     * @return un Optional con el elemento de sincronización.
     */
    public Optional<Phaser> unirseVotacion() {
        Optional<Phaser> resultado = Optional.empty();

        exm.lock();
        try {
            if (numModeradores > 0) {
                numModeradores--;
                resultado = Optional.of(sincronizacion);
            }
        } finally {
            exm.unlock();
        }

        return resultado ;
    }

    /**
     * Cuando un moderador finalice su participación en la votación deberá
     * incluir el resultado de la decisión que ha organizado.
     * @param decision decisión del moderador que se añade al resultado de la
     *                 votación.
     */
    public void enviarDecision(Decision decision) {
        resultadoVotacion.add(decision);
    }

    @Override
    public String toString() {
        String resultado = "********** VOTACIÓN(" + iD + ") **********";

        resultado += "\n\tUmbral de decisión " + umbral + "\t Estado de la votación " + estadoVotacion;

        for (Decision decision : resultadoVotacion) {
            resultado += "\n\t---------- " + decision.getiD() + "----------\n\t" + decision;
        }

        return resultado;
    }
}
