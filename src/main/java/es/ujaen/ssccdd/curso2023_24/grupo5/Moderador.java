package es.ujaen.ssccdd.curso2023_24.grupo5;

import java.util.*;
import java.util.concurrent.Phaser;
import java.util.concurrent.locks.Lock;

import static es.ujaen.ssccdd.curso2023_24.grupo5.Constantes.*;


public class Moderador implements Runnable {
    private final String iD;
    private final List<Votacion> listaVotaciones;
    private final List<Nodo> listaNodos;
    private final Lock exmVotacion;
    private final Lock exmNodos;
    private Votacion votacionActual;
    private Optional<Phaser> sincVotacion;
    private int numVotaciones;

    public Moderador(String iD, List<Votacion> listaVotaciones, List<Nodo> listaNodos, Lock exmVotacion,
                     Lock exmNodos) {
        this.iD = iD;
        this.listaVotaciones = listaVotaciones;
        this.listaNodos = listaNodos;
        this.exmVotacion = exmVotacion;
        this.exmNodos = exmNodos;
        this.numVotaciones = aleatorio.nextInt(MIN_VOTACIONES,MAX_VOTACIONES);
    }

    public String getiD() {
        return iD;
    }
    @Override
    public void run() {
        System.out.println("Hilo("+iD+") comienza su ejecución");

        try {
            Decision enCurso;

            while ( numVotaciones > 0 ){
                obtenerVotacion();
                if ( sincVotacion.isPresent() ) {
                    enCurso = inicioVotacion(sincVotacion.get());
                    realizarVotacion(sincVotacion.get(), enCurso);
                }
            }

            System.out.println("Hilo("+iD+") ha FINALIZADO correctamente");
        } catch (InterruptedException e) {
            System.out.println("Hilo("+iD+") ha sido INTERRUMPIDO");
        }
    }

    /**
     * Recorre la lista de votaciones hasta encontrar la primera en la que se puede obtener
     * el elemento de sincronización que permite participar en la votación.
     * @throws InterruptedException
     */
    private void obtenerVotacion() throws InterruptedException {
        sincVotacion = Optional.empty();

        exmVotacion.lockInterruptibly();
        try {
            Iterator<Votacion> it = listaVotaciones.iterator();

            while ( it.hasNext() && sincVotacion.isEmpty() ) {
                votacionActual = it.next();
                sincVotacion = votacionActual.unirseVotacion();
            }
        } finally {
            exmVotacion.unlock();
        }
    }

    /**
     * Crea la decisión para la votación y se sincroniza con el gestor y el resto de moderadores
     * para empezar las rondas de votación.
     * @param sinc para sincronizar la votación
     * @throws InterruptedException
     */
    private Decision inicioVotacion(Phaser sinc) throws InterruptedException {
        Decision nuevaDecision = crearDecision();

        System.out.println("Hilo(" + iD + ") a la espera para comenzar la votación para "
                            + votacionActual.getiD());
        sinc.awaitAdvanceInterruptibly(sinc.arrive());

        return  nuevaDecision;
    }

    /**
     * Realiza las rondas de votación de forma sincronizada con el resto de moderadores y el gestor de
     * la misma
     * @param sinc elemento de sincronización para las diferentes rondas
     * @param enCurso decisión que forma parte de la votación actual
     * @throws InterruptedException
     */
    private void realizarVotacion(Phaser sinc, Decision enCurso) throws InterruptedException {
        EstadoConsenso estadoInicial;

        do {
            estadoInicial = enCurso.getEstadoActual();
            enCurso.avanceDecision();
            if ( estadoInicial.equals(enCurso.getEstadoActual()) ) {
                System.out.println("Hilo("+iD+") no puede completar su decisión para " + votacionActual.getiD());
                votacionActual.enviarDecision(enCurso);
                sinc.arriveAndDeregister();
            } else {
                System.out.println("Hilo(" + iD + " avanza a la siguiente etapa " + votacionActual.getiD());
                sinc.awaitAdvanceInterruptibly(sinc.arrive());
            }
        } while ( !enCurso.completada() && !estadoInicial.equals(enCurso.getEstadoActual()) );

        if ( enCurso.completada() ) {
            System.out.println("Hilo("+iD+") ha COMPLETADO su decisión para " + votacionActual.getiD());
            votacionActual.enviarDecision(enCurso);
            numVotaciones--;
        }
    }

    /**
     * Creamos la decisión para la votación asignada
     * @throws InterruptedException
     */
    private Decision crearDecision() throws InterruptedException {
        List<Nodo> nodosDecision;

        nodosDecision = obtenerNodos();

        System.out.println("Hilo("+iD+") decisión creada para " + votacionActual.getiD());

        return new Decision(iD+"-Decision-"+votacionActual.getiD(),
                nodosDecision, votacionActual.getUmbral());
    }

    /**
     * Obtiene el número de nodos necesarios para la decisión de la lista compartida.
     * Entra en espera ocupada hasta que haya el número suficiente de nodos.
     * @return la lista de nodos para la decisión
     */
    private List<Nodo> obtenerNodos() throws InterruptedException {
        List<Nodo> resultado = new ArrayList<>();
        int numNodos = votacionActual.getMinNodos();

        while ( resultado.size() != numNodos ) {
            exmNodos.lockInterruptibly();
            try {
                if (listaNodos.size() >= numNodos)
                    for (int i = 0; i < numNodos; i++)
                        resultado.add(listaNodos.remove(PRIMERO));
            } finally {
                exmNodos.unlock();
            }
        }

        return resultado;
    }
}
