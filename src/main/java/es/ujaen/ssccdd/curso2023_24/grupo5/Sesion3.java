package es.ujaen.ssccdd.curso2023_24.grupo5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static es.ujaen.ssccdd.curso2023_24.grupo5.Constantes.*;

public class Sesion3 {
    private static final int MIN_GESTORES = 2;
    private static final int MAX_GESTORES = 5;
    private static final int TIEMPO_CICLO_MAIN = 2; // segundos
    private static final int TIEMPO_EJECUCION = 30; // segundos
    private static final int NUM_NODOS = 6;
    private static final int MIN_MODERADORES = 6;
    private static final int MAX_MODERADORES = 11;

    public static void main(String[] args) throws InterruptedException {
        // Declaración de variables
        Thread[] listaTareas;
        Gestor gestor;
        Moderador moderador;
        Lock exmVotacion;
        Lock exmNodos;
        List<Votacion> listaVotaciones;
        List<Nodo> listaNodos;
        int numGestores;
        int numModeradores;

        // Inicialización de variables
        exmVotacion = new ReentrantLock();
        exmNodos = new ReentrantLock();
        listaNodos = new ArrayList<>();
        listaVotaciones = new ArrayList<>();
        numGestores = aleatorio.nextInt(MIN_GESTORES,MAX_GESTORES);
        numModeradores = aleatorio.nextInt(MIN_MODERADORES,MAX_MODERADORES);
        listaTareas = new Thread[numGestores+numModeradores];
        for (int i = 0; i < numGestores; i++) {
            gestor = new Gestor("Gestor("+i+")",listaVotaciones,exmVotacion);
            listaTareas[i] = new Thread(gestor, gestor.getiD());
        }
        for (int i = numGestores; i < listaTareas.length; i++) {
            moderador = new Moderador("Moderador("+i+")",listaVotaciones,listaNodos,exmVotacion, exmNodos);
            listaTareas[i] = new Thread(moderador, moderador.getiD());
        }

        // Cuerpo de ejecución
        System.out.println("Hilo(Principal) Comienza su ejecución para");
        Arrays.stream(listaTareas).forEach(Thread::start);

        int numCiclos = TIEMPO_EJECUCION / TIEMPO_CICLO_MAIN;
        for (int i = 0; i < numCiclos ; i++) {
            int numNodos = aleatorio.nextInt(NUM_NODOS) + 1;
            for (int j = 0; j < numNodos; j++) {
                exmNodos.lock();
                try {
                    listaNodos.add(new Nodo("Nodo(" + i + "," + j + ")", Comportamiento.getComportamiento()));
                } finally {
                    exmNodos.unlock();
                }
            }
            System.out.println("Hilo(Principal) se han añadido " + numNodos + " nuevos nodos en el ciclo " + i);

            TimeUnit.SECONDS.sleep(TIEMPO_CICLO_MAIN);
        }

        Arrays.stream(listaTareas).forEach(Thread::interrupt);
        for (Thread tarea : listaTareas)
            tarea.join();

        // Presentar resultados
        System.out.println("\n---------- RESULTADOS ----------\n");
        listaVotaciones.forEach(votacion ->
                System.out.println("********** " + votacion.getiD() + " **********\n\t"+votacion));
        System.out.println("\n----------- FIN RESULTADOS ----------\n");

        // Finalización
        System.out.println("Hilo(Principal) Ha finalizado");
    }
}