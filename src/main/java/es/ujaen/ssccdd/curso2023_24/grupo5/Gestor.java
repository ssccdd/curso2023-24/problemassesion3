package es.ujaen.ssccdd.curso2023_24.grupo5;

import java.util.List;
import java.util.concurrent.Phaser;
import java.util.concurrent.locks.Lock;

import static es.ujaen.ssccdd.curso2023_24.grupo5.Constantes.*;
import static es.ujaen.ssccdd.curso2023_24.grupo5.Constantes.EstadoConsenso.*;

public class Gestor implements Runnable {
    private static final int MIN_UMBRAL = 75;
    private static final int MAX_UMBRAL = 86;
    private final String iD;
    private final List<Votacion> listaVotaciones;
    private final Lock exm;
    private Votacion votacion;
    private Phaser sincVotacion;

    public Gestor(String iD, List<Votacion> listaVotaciones, Lock exm) {
        this.iD = iD;
        this.listaVotaciones = listaVotaciones;
        this.exm = exm;
    }

    public String getiD() {
        return iD;
    }

    @Override
    public void run() {
        System.out.println("Hilo("+iD+") comienza su ejecución");

        int numVotaciones = aleatorio.nextInt(MIN_VOTACIONES,MAX_VOTACIONES);

        try {
            for (int i = 0; i < numVotaciones; i++) {
                crearVotacion(i);
                inicioVotacion();
                completarVotacion();
            }

            System.out.println("Hilo("+iD+") ha FINALIZADO correctamente");
        } catch (InterruptedException e) {
            System.out.println("Hilo("+iD+") ha sido INTERRUMPIDO");
        }
    }

    /**
     * Crea una nueva votación con los parámetros necesarios para que los moderadores
     * puedan unirse a ella para completar sus decisiones y dejar el resultado.
     * Se añade a la lista compartida de votaciones y se registra en el phaser de sincronización
     * @param indice número de votación que tiene que creear
     */
    private void crearVotacion(int indice) {
        int numNodos = aleatorio.nextInt(MIN_NODOS, MAX_NODOS);
        int umbral = aleatorio.nextInt(MIN_UMBRAL,MAX_UMBRAL);
        int numModeradores = aleatorio.nextInt(MIN_MODERADORES,MAX_MODERADORES);
        sincVotacion = new Phaser(numModeradores);
        votacion = new Votacion("Votacion("+indice+"-"+iD+")",numNodos,umbral,numModeradores,sincVotacion);
        sincVotacion.register(); // Para sincronizarse con la votación creada
        exm.lock();
        try {
            listaVotaciones.add(votacion);
        } finally {
            exm.unlock();
        }

        System.out.println("Hilo("+iD+") creada " + votacion.getiD() + " y a la espera de " + numModeradores +
                " moderadores");
    }

    /**
     * Los moderadores y el gestor se sincronizan para dar inicio a las rondas de votación
     * @throws InterruptedException
     */
    private void inicioVotacion() throws InterruptedException {
        System.out.println("Hilo("+iD+") a la espera de que los moderadores estén listos para la votación " +
                            votacion.getiD());
        sincVotacion.awaitAdvanceInterruptibly(sincVotacion.arrive());
    }

    /**
     * El gestor tiene que esperar que las decisiones asociadas avancen por los diferentes estados
     * disponibles hasta su finalización o hasta que no queden moderadores.
     * @throws InterruptedException
     */
    private void completarVotacion() throws InterruptedException {
        int etapaVotacion = PRIMERO;

        while ( !estadosDecision[etapaVotacion].equals(FINALIZADO) ) {
            System.out.println("Hilo("+iD+") espera en el estado " + estadosDecision[etapaVotacion] +
                    " de la votación " + votacion.getiD());
            sincVotacion.awaitAdvanceInterruptibly(sincVotacion.arrive());
            etapaVotacion++;
            votacion.setEstadoVotacion(estadosDecision[etapaVotacion]);
        }

        System.out.println("Hilo("+iD+") ha COMPLETADO la votación " + votacion.getiD());
    }
}
