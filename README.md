﻿[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas

## Sesión 3

Problemas propuestos para la Sesión 3 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2023-24.

Los ejercicios son diferentes para cada grupo:

Los **objetivos** de la práctica son:

-   Definir adecuadamente las clases que implementan lo que se ejcutará en los _hilos_. Programando adecuadamente la excepción de interrupción de su ejecución.
-   Utilizar adecuadamente la herramienta de sincronización propuesta para la resolución del ejercicio. Solo estará permitido utilizar objetos de esa herramienta y ningún otro.
-   Demostrar que la ejecución de los hilos es correcta.

Los ejercicios son diferentes para cada grupo:

-   [Grupo 1 y 2](#grupo-1-y-2)
-   [Grupo 3](#grupo-3)
-   [Grupo 4](#grupo-4)
-   [Grupo 5](#grupo-5)

### Grupo 1y2
En una biblioteca de Jaén, se busca implementar un sistema propio de préstamo de libros utilizando la herramienta de sincronización  [CyclicBarrier](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/concurrent/CyclicBarrier.html). Añadir todas las constantes necesarias en la interfaz "**Constantes**" para la resolución del ejercicio. Para el ejercicio también hay que utilizar  [Lock](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/concurrent/locks/Lock.html)  para asegurar la exclusión mutua de las variables compartidas.

**Clase Usuario:**

-   **Atributos**:
    

-   Identificador (id).
    
-   Número máximo de libros que puede llevar prestados (maxLibrosPrestados).
    
-   Tiempo máximo que tiene para devolver los libros (tiempoDevolucion).
    
-   Lista de libros actualmente prestados (librosPrestados).
    
-   Tipo: ESTÁNDAR (3 libros prestados máximo), PREMIUM (5 libros prestados máximo).
    

-   **Métodos:**
    

-   Constructor.
    
-   Métodos de acceso para obtener los atributos ("getId()", "getMaxLibrosPrestados()", "getTiempoDevolucion()").
    
-   Método "prestarLibro(Libro libro)" para simular el préstamo de un libro.
    
-   Método "devolverLibros(List<Libro> libros)" para simular la devolución de libros.
    

**Clase Libro:**

-   **Atributos:**
    

-   Identificador (id).
    
-   Título del libro (titulo).
    

-   **Métodos:**
    

-   Constructor.
    
-   Método "toString()" para representar un objeto de la clase.
    

**Clase GeneradorPrestamos:**

-   **Atributos**:
    -   Nombre
    -   Lista de préstamos disponibles compartida con su gestor
    -   _**Los elementos de sincronización ("CyclicBarrier") necesarios**_
-   **Ejecución**:
    -   Hasta que sea interrumpido:
        -   Esperará a que el gestor le indique que necesita más préstamos y generará una cantidad variable entre 1 y 3 préstamos.
        -   Preparar un préstamo necesita un tiempo de entre 1 y 5 segundos.
        -   Avisará a su gestor que el préstamo está disponible.

**Clase GestorPrestamos:**

-   **Atributos:**
    

-   Nombre del gestor.
    
-   Lista compartida de usuarios entre todos los gestores.
    
-   Lista compartida de préstamos disponibles.
-   Lista de préstamos realizados por ese gestor en concreto.
    
-   **_Elementos de sincronización ("CyclicBarrier") que sean necesarios._**
    

-   **Ejecución de la tarea:**
    
    -   Esperará hasta que el Hilo Principal de la señal que puede iniciar su trabajo.
        
    -   Crearán 2 GeneradorDePrestamos (generadores de préstamos) y los ejecutará con los elementos compartidos necesarios.
        
    -   Hasta que sea interrumpido o finalice su ejecución:  
        -   Se obtendrá un usuario de la lista compartida de usuarios disponibles. Si no hay usuarios disponibles, se seguirá intentando hasta que no haya más peticiones de préstamos disponibles.
            
        -   Una vez seleccionado un usuario, se realizará un préstamo. Para ello, se selecciona el número de libros necesarios para ese préstamo (cada usuario tiene un máximo).
            
        -   Si no hay préstamos disponibles, se avisará a los generadores de préstamos para que creen más préstamos. Esperará hasta que todos los generadores de préstamos le indiquen que ya tienen disponibles los libros.
            
        -   Asignar los libros correspondientes al usuario y almacenar el préstamo en la lista de préstamos realizados del gestor. El tiempo de asignación del préstamo será de entre 2 y 5 segundos.
            
        -   Antes de finalizar, se almacenan los préstamos completados en la lista de préstamos del gestor correspondiente.
            
        -   La tarea deberá indicar si ha finalizado su ejecución o si ha sido solicitada su interrupción en su finalización.
            

**Hilo Principal:**

-   Creará 4 “GestorPrestamos” y se les asociarán los hilos para la ejecución. Se deben crear las variables compartidas necesarias para estos gestores.
    
-   Cada gestor podrá atender entre 3 y 6 préstamos.
    
-   El hilo principal creará 4 usuarios. El tiempo de préstamo será de entre 2 y 5 segundos.
    
-   Se inicia la ejecución de los gestores y se espera a que finalice el primero. En ese momento, se interrumpen el resto de gestores y se espera su finalización.
    
-   Antes de finalizar, se debe presentar la lista de los préstamos que cada uno de los gestores ha realizado durante su ejecución.
    

**Consideraciones Adicionales:**
-   Asegurar que un usuario no pueda llevar prestados más libros de los permitidos por su límite.
-   Un préstamo está formado por el libro a prestar y, tras ser prestado, por el usuario al que se le presta.
-   Presentar mensajes informativos para cada operación realizada o descartada.


### Grupo 3
La universidad quiere crear una aplicación para el uso de bicicletas para la comunidad universitaria y así fomentar el transporte sostenible. En esta práctica la universidad ha contratado los servicios de diferentes agencias de viajes que organizan excursiones con los usuarios. El objetivo es que la experiencia de los viajes sea compartida y la satisfacción global de los usuarios. La herramienta de sincronización que se utilizará en el ejercicio es  [Phaser](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/concurrent/Phaser.html)  para las diferentes tareas que compondrán el ejercicio. Además se tiene que utilizar  [Lock](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/concurrent/locks/Lock.html)  para resolver los posibles problemas de exclusión mutua que haya en la resolución del ejercicio. Para ello se deberán realizar las siguientes tareas:

-   Añadir todas las constantes globales al proyecto necesarias en la interface  **Constantes**  para la resolución del ejercicio. Si las constantes solo afectan a una clase se pueden definir dentro del archivo para esa clase.
-   **Viaje**: Esta clase contiene la información necesaria para poder ogranizar un viaje. El elemento necesario para la sincronización, la estación de bicicletas que formará parte del viaje, las etapas del viaje y el número de usuarios para el viaje. Resultado del viaje de cada usuario que se haya apuntado.
-   **Usuario**: Será una tarea que simula la actividad de un miembro de la comunidad universitaria que participara de las actividades organizadas por una agencia de viajes.
    -   _Atributos_
        -   Una nombre único para el sistema
        -   Lista compartida para los viajes
        -   Elemento de exclusión mutua para acceder a los viajes
    -   _Ejecución_
        -   Debe obtener de la lista de actividades uno de los viajes para apuntarse donde aún quede sitio.
        -   Antes de comenzar el viaje hay que asegurarse que el resto de usuarios que comparten viaje están listos para iniciarlo, es decir, todos han recogido su bicicleta.
        -   Cada vez que se completa una etapa del viaje el usuario deberá comprobar si se ha producido una incidencia. Si se produce una incidencia debe devolver la bicicleta a la estación y dejar el viaje.
        -   Cuando se completa el viaje deberá devolver la bicicleta.
        -   Cada ciclo del viaje será de un segundo.
        -   La tarea puede interrumpirse, en cualquier caso, deberá completar el resultado del viaje antes de finalizar.
-   **AgenciaViajes**: Estará contratada por la universidad para organizar actividades con los usuarios de la comunidad.
    -   _Atributos_
        -   Un nombre único para el sistema
        -   Lista compartida para los viajes
        -   Elemento de exclusion mutua para acceder a los viajes
    -   _Ejecucion_  
        -   En el constructor tendrá que crear una estación de bicicletas asociada para todos los viajes que cree en su ejecución.
        -   Crear un viaje y añadirlo a la lista para que los usuarios lo completen. El número de usuarios para el viaje dependerá del número de bicicletas disponibles en la estación en todo momento.
        -   La agencia controrará las etapas de cada viaje, es decir, debe formar parte de las etapas de ese viaje.
        -   Hasta que no se ha completado un viaje no crea el siguiente. El número de viajes que creará será de entre 3 y 5.
        -   Debe programarse la interrupción de la tarea que podrá finalizar antes de completar todos sus viajes.
-   **HiloPrincipal**:
    -   Se crean entre 10 y 15 usuarios y entre 2 y 4 agencias de viaje. Deben tener todas las variables compartidas asociadas antes de iniciar su ejecución.
    -   Espera por 30 segundos antes de solicitar la interrupción de las tareas.
    -   Debe esperar a que todas las tareas finalicen su ejecución.
    -   Antes de finalizar se debe presentar el resultado de los viajes.

### Grupo 4
Necesitamos organizar diferentes partidas entre jugadores atendiendo a categorías y tiempo que lleva la realización de una partida. La herramienta de sincronización que se utilizará en el ejercicio es [CyclicBarrier](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/concurrent/CyclicBarrier.html) para las diferentes tareas que compondrán el ejercicio. También hay que utilizar  [Lock](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/concurrent/locks/Lock.html)  para garantizar la exclusion mutua de las variables compartidas. Para ello se deberán realizar las siguientes tareas:

-   Añadir todas las constantes necesarias en la interface  **Constantes**  para la resolución del ejercicio. Además hay que añadir las constantes que tengas un ámbito de ejecución asociado al proyecto, en otro caso se definirán en el archivo de la clase asociada.
-   **Partida**: Tendrá los datos necesarios para poder jugar una partida de una categoría en concreto con un número de usuarios que dependerá del número de copias que se tengan del juego. También tendrá el elemento necesario para la sincronización don los organizadores y plataformas. En el resultado de la partida se deberá indicar si se ha completado para el tiempo máximo asociado a la categoría y los jugadores que han participado.
-   **PlataformaJuegos**: Esta tarea creará partidas y se coordinará con el organizador para el desarrollo de la misma.
    -   _Atributos_
        -   Nombre de la plataforma que debe ser único en el sistema.
        -   Lista con las partidas que estarán compartidas
        -   Elemento para la exclusion mutua
    -   _Ejecución_
        -   Creará una partida para una categoría con una cantidad de juegos para los usuarios que formen parte y el elemento de sincronización necesario.
        -   No se creará una nueva partida hasta que no se haya completado el que se ha creado.
        -   El número de partidas que deberá completar la plataforma será de entre 3 a 6.
        -   La tarea deberá indicar si ha finalizado su ejecución o si ha sido solicitada la interrupción en su finalización.
-   **OrganizadorEventos**  : es el encargado de encontrar a los usuarios que participarán en el juego y completará el resultado del mismo
    -   _Atributos_
        -   Nombre que debe ser único en el sistema.
        -   Lista con las partidas que estarán compartidas
        -   Lista con los jugadores disponibles para los organizadores
        -   Elemento para la exclusion mutua
    -   _Ejecución_
        -   Obtendrá una de las partidas disponibles y encontrará jugadores que puedan participar.
        -   Cuando tenga a los jugadores simulará el inicio del juego por un tiempo aleatorio asociado a la categoría. El mínmo será un 60% y el máximo un 120%.
        -   Informará de su finalización a la plataforma completando la información de su desarrollo en el juego.
        -   El número de juegos que debe completar el organizador será de entre 4 a 8.
        -   También debe implementarse la interrupción de la tarea.
-   **HiloPrincipal**:
    -   Crear entre 3 y 5 plataformas de juegos y entre 2 y 4 organizadores. Les asocia los hilos para su ejecución. Se deben crear las variables compartidas necesarias para estos gestores.
    -   Creará una cantidad de hasta 5 usuarios cada segundo.
    -   Pasados 30 segundos pedirá la finalización de las tareas y esperará a su finalización.
    -   Antes de terminar mostrará el resultado de todos los juegos creados.

### Grupo 5
Se pretende establecer un sistema de debate para tomar decisiones donde participe un número de nodos que valorarán las decisiones a adoptar. La herramienta de sincronización que se utilizará en el ejercicio es [Phaser](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/concurrent/Phaser.html) para las diferentes tareas que compondrán el ejercicio. Además se tiene que utilizar [Lock](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/concurrent/locks/Lock.html) para resolver los posibles problemas de exclusión mutua que haya en la resolución del ejercicio. Para ello se deberán realizar las siguientes tareas:

-   Añadir todas las constantes necesarias en la interface **Constantes** para la resolución del ejercicio. Además hay que añadir las constantes que tengas un ámbito de ejecución asociado al proyecto, en otro caso se definirán en el archivo de la clase asociada.
-   **Votacion**: Almacenará los elementos necesarios para completar la votación. Tendrá un identificador la que identifique, un número mínimo de nodos, el umbral y un número de moderadores que participarán en la votación. Además de los elementos de sincronización necesarios. El resultado será las decisiones que se han completado, cada moderador será el encargado de una decisión.
-   **Moderador**: Representa el encargado de coordinar diferentes decisiones que tenga asociadas así como los nodos que participan en ellas.
    -   _Atributos_:
        -   Identificador único dentro del sistema.
        -   Lista compartida votaciones disponibles.
        -   Lista compartida de nodos en el sistema.
        -   Elementos de exclusión mutua
    -   Ejecución:
        -   Obtendrá una votación en la que participar, es decir, donde aún se puedan apuntar moderadores.
        -   Deberá crear una decisión y buscar una cantidad de nodos de como máximo el 10% adicional al número mínimo de nodos.
        -   Todos los moderadores que participan en una votación deberán pedir los avances en su decisión asociada de forma sincronizada, es decir, no se podrá pedir un nuevo avance hasta que todos no hayan completado el último. Esto tendrá un tiempo de 1 a 2 segundos.
        -   Si no se produce un avance en la decisión el moderador deberá dejar de participar en los avances. Deberá dejar anotado el estado de su decisón el la votación correspondiente.
        -   Cuando deje una votación podrá participar de una nueva votación.
        -   Debe completar de 3 a 5 votaciones o hasta que sea interrumpido.
-   **Gestor**: Es el encargado de ir creando decisiones en las que participarán los moderadores.
    -   _Atributos_  
        -   Identificador único dentro del sistema.
        -   Lista compartida votaciones disponibles.
        -   Elementos de exclusión mutua
    -   _Ejecución_
        -   Creará una decisión con un número mínomo de nodos de entre 5 a 8 y con un umbral de entre el 75% al 85%. Los moderadores que deben participar será de entre 3 a 5.
        -   No creará una nueva votación hasta que no haya finalizado la actual.
        -   Deberá crear un total de entre 3 a 6 votaciones o hasta que sea interrumpido.
-   **HiloPrincipal**:
    -   Se crean de entre 2 a 4 gestores y de entre 6 a 10 moderadores, los datos compartidos necesarios y se les asocian los hilos para su ejecución.
    -   En cada ciclo creará hasta 5 nodos.
    -   Pasados 30 segundos solicitará la finalización de todas las tareas.
    -   Esperará a que todas finalicen.
    -   Antes de finalizar presentar los estados de todas las votaciones creadas.
